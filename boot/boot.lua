--[[
  This is the main boot code.
  
  purpose:
  * load the kernel
  * catch and report kernel errors
--]]
rawset(_G, "_loadfile", loadfile)
loadfile = function(file, name)
  if name then
    local f = fs.open(file, "r")
    return loadstring(f.readAll(), name)
  else
    return _loadfile(file)
  end
end

rawset(_G, "loadfile", loadfile)

local s, m = loadfile("/sbin/kernel.lua", "Kernel")

if not s then error("Could not load Kernel: "..m) end

s()

shell.exit()