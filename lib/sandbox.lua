local COREAPIS = {"redstone", "peripheral", "bit", "coroutine", "math", "fs", "os", "string", "table",
"type", "setfenv", "loadstring", "pairs", "rawequal", "ipairs", "xpcall", 
"read", "unpack", "setmetatable", "rawset", "rs", "http", "rawget", "printError",
"sleep", "assert", "error", "tostring", "next", "tonumber", "loadfile", "write", "print",
"select", "term", "pcall",


"stringutils", "table_", "tweaks",
}

local function copy(t, tr)
  if type(t) == "table" then
    local ret = {}
    for k, v in pairs(t) do
      if not tr[v] then 
        tr[v] = true
        ret[k] = copy(v, tr)
      end
    end
    return ret
  else
    return t
  end
end

function makeSandbox(_user)
  if not _user then _user = user end
  local s = {}
  for a = 1, #COREAPIS do
    s[COREAPIS[a]] = copy(_G[COREAPIS[a]], {})
  end
  s.os = {
    version = KilOS.version,
    computerID = os.computerID,
    getComputerLabel = os.getComputerLabel,
    setComputerLabel = function(label)
      if _user.getName() == "root" then
        os.setComputerLabel(_user)
        return true
      else
        return false, "You are no root"
      end
    end,
    pullEvent = os.pullEvent,
    pullEventRaw = os.pullEventRaw,
    queueEvent = os.queueEvent,
    clock = os.clock,
    startTimer = os.startTimer,
    time = os.time,
    setAlarm = os.setAlarm,
    shutdown = os.shutdown,
    reboot = os.reboot
  }
  
  s.KilOS = {
    getVersion = KilOS.getVersion,
    getUpdateUrl = KilOS.getUpdateUrl
  }
  s.getfenv = function()
    return s
  end
  
  s.dofile = function(file)
    local f = loadfile(file)
    setfenv(f, s)
    return f()
  end
  
  s.user = _user
  
  s._G = s
  
  return s
end

