function getFreeSpot(t)
  for a = 1, #t + 1 do
    if not t[a] then return a end
  end
end

function getKey(t, v)
  for k, v2 in pairs(t) do
    if v == v2 then return k end
  end
end

function pack(...)
  return {...}
end