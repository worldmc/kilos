local user = "system"
local read, write, execute = 4, 2, 1
local function getAMode(file)
  if not fs.exists(file) then
    fs.open(file)
  end
  perms = textutils.unserialize(fs.open(file:sub(#fs.getName(file) * -1 - 1).."/.perms", "r"))
  local n = fs.getName(file)
  if not perms[n] then
    perms[n] = {owner = user, mode = 744}
  end
  mode = tostring(perms[n].mode)
  mode = ("0"):rep(3 - #mode)..mode
  local user, group, other = tonumber(mode:sub(1,1)), tonumber(mode:sub(2,2)), tonumber(mode:sub(3,3))
  --todo save file again
  return {
    --[[user = {
      r = bit.and(user, read) != 0,
      w = bit.and(user, write) != 0,
      x = bit.and(user, execute) != 0,
    },
    group = {
      r = bit.and(group, read) != 0,
      w = bit.and(group, write) != 0,
      x = bit.and(group, execute) != 0,
    },
    other = {
      r = bit.and(other, read) != 0,
      w = bit.and(other, write) != 0,
      x = bit.and(other, execute) != 0,
    },
    owner = perms[n].owner--]]
  }
end

--function getPermsR(file)

function list(path, includeHidden)
  
end

function exists(path)

end