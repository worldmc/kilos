local name = nil
local home = "/"
local userlist = {}

function getName() return name end
function getHome() return home end

local function save()
  local f = fs.open("/etc/users", "w")
  f.write(textutils.serialize(userlist))
  f.close()
end

function login(user, pass)
  if not pass then pass = "" end
  if userlist[user] and ((not userlist[user]['pass']) or userlist[user]['pass'] == stringutils.SHA1(pass)) then
    local initial = false
    if not name then initial = true end
    home = userlist[user]['home']
    if home:sub(-1, -1) == "/" then home = home:sub(1, -2) end
    name = user
    return true
  end
  return false
end

function newUser(user, homedir)
  if name ~= "root" then
    error ("You are not root.")
  end
  if userlist[user] then
    error("user already exists")
  end
  userlist[user] = {home = (homedir or "/home/"..user), lastrootpw = false}
  if not fs.exists(userlist[user].home) then
    fs.makeDir(userlist[user].home)
  end
  save()
end

function setPass(user, pass)
  if name ~= "root" then
    error ("You are not root.")
  end
  if not userlist[user] then
    error("user does not exists")
  end
  if pass then
    userlist[user].pass = stringutils.SHA1(pass)
  else
    userlist[user].pass = nil
  end
  userlist[user].rootpwchg = true
  save()
end

function setPassOwn(old, new)
  if not userlist[name].pass == stringutils.SHA1(old) then
    error("Wrong password")
  end
  userlist[name].pass = stringutils.SHA1(new)
  userlist[name].rootpwchg = false
  save()
end

function needsPass(user)
  if not userlist[user]  then return true end
  return not (userlist[user].pass == nil or userlist[user].pass == stringutils.SHA1(""))
end

function lock(autoroot)
  local pe = os.pullEvent
  os.pullEvent = os.pullEventRaw
  tweaks.setColor(colors.white, colors.black)
  while true do
    local u
    --print(#userlist, textutils.serialize(userlist))
    local nu = 0
    for k, v in pairs(userlist) do nu = 1 + nu end
    if autoroot and (nu == 1) and (not needsPass("root")) then
      u = "root"
    end
    if name then 
      print ("login for "..name)
      u = name
    elseif u then
    
    else
      write("login: ")
      u = read()
    end
    local p
    if needsPass(u) then
      write((u or name).."@"..(os.getComputerLabel() or "unnamed #"..os.getComputerID()).."'s password: ")
      p = read("*")
    else
      p = ""
    end
    if login(u, p) then
      print("Logged in as "..u..".")
      break
    else
      print("Invalid login or password")
    end
  end
  os.pullEvent = pe
end

function logout()
  user = nil
  home = "/"
  lock()
end

function delUser(user)
  if name ~= "root" then
    error ("You are not root.")
  end
  if not userlist[user] then
    error("user does not exists")
  end
  if user == "root" then
    error("Con't remove root")
  end
  userlist[user] = nil
  save()
end

function init()
  if not fs.exists("/etc") then fs.makeDir("/etc") end
  if not fs.exists("/etc/users") then local f = fs.open("/etc/users", "w") f.write("{}") f.close() end
  local f = fs.open("/etc/users", "r")
  userlist = textutils.unserialize(f.readAll())
  f.close()
  if not userlist.root then
    --print("no root")
    userlist.root = {home = "/root"}
    if not fs.exists("/root") then fs.makeDir("/root") end
  end
  save()
end