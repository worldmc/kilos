local tasks = {}
local ids = {}
local globalEvents = {}

function newTask(func)
  local c = coroutine.create(func)
  local id = table_.returnFreeSpot(tasks)
  tasks[id] = {thread = c, events = {}, handlers = {}}
end

function killTask(id)
  if not tasks[id] then return false end
  tasks[id] = nil
  table.remove(ids, table_.getKey(ids, id))
  --events.raise("taskkill", id)
  return true
end

function terminateTask(id)
  if not tasks[id] then return false end
  table.insert(tasks[id].events, {"terminate"})
  return true
end

function endTask(id)

end

function queueEvent(id, event, ...)
  if type(event) ~= "string" then error("Parameter #2: string expected, got "..type(event)) end
  if id then
    if not tasks[id] then return false end
    table.insert(tasks[id].events, table_.pack(event, ...))
  end
end

local bExit = false

function run()
  while not bExit do
    for a = 1, #ids do
      local id = ids[a]
      if tasks[id].events[1] ~= nil then
        -- event scheduled
        tasks[id].handlers = pack(coroutine.resume(tasks[id].thread, unpack(tasks[id].events[1])))
        table.remove(tasks[id].events[1], 1)
        if coroutine.status(tasks[id].thread == "dead") then
          --events.raise("taskend", id)
          tasks[id] = nil
          table.remove(ids, table_.getKey(ids, id))
        end
      else
        for b = 1, #globalEvents do
          local key = table_.getKey(globalEvents[b][1])
          if key then
            tasks[id].handlers = pack(coroutine.resume(tasks[id].thread, unpack(globalEvents[key])))
            if coroutine.status(tasks[id].thread == "dead") then
              --events.raise("taskend", id)
              tasks[id] = nil
              table.remove(ids, table_.getKey(ids, id))
            end
          end
        end
      end
    end
    -- fetch new events
    os.queueEvent("end_of_event_queue")
    local e = ""
    while true do
      local ev = {os.pullEventRaw}
      e = ev[1]
      if e == "end_of_event_queue" then break end
      table.insert(globalEvents, ev)
    end
  end
end