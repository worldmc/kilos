function os.run( _tEnv, _sPath, ... )
    local tArgs = { ... }
    local fnFile, err = loadfile( _sPath )
    if fnFile then
        local tEnv = _tEnv
        tEnv.getfenv = function() return _tEnv end
        --setmetatable( tEnv, { __index = function(t,k) return _G[k] end } )
		setmetatable( tEnv, { __index = _G } )
        setfenv( fnFile, tEnv )
        local ok, err = pcall( function()
        	fnFile( unpack( tArgs ) )
        end )
        if not ok then
        	if err and err ~= "" then
	        	printError( err )
	        end
        	return false
        end
        return true
    end
    if err and err ~= "" then
		printError( err )
	end
    return false
end

local tAPIsLoading = {}
function os.loadAPI( _sPath, _bStripExt )
	local sName = fs.getName( _sPath )
	if tAPIsLoading[sName] == true then
		printError( "API "..sName.." is already being loaded" )
		return false
	end
	tAPIsLoading[sName] = true
		
	local tEnv = {}
	tEnv.getfenv = function() return _tEnv end
	setmetatable( tEnv, { __index = _G } )
	local fnAPI, err = loadfile( _sPath )
	if fnAPI then
		setfenv( fnAPI, tEnv )
		fnAPI()
	else
		printError( err )
        tAPIsLoading[sName] = nil
		return false
	end
	
	local tAPI = {}
	for k,v in pairs( tEnv ) do
		tAPI[k] =  v
	end
	
	if _bStripExt and #sName > 4 and sName:sub(-4, -1) == ".lua" then
	  sName = sName:sub(1, -5)
	end
	
	_G[sName] = tAPI	
	tAPIsLoading[sName] = nil
	return true
end

local osetfenv = setfenv
function setfenv(func, env)
  env.getfenv = function() return env end
  osetfenv(func, env)
end

function os.unloadAPI( _sName )
	if _sName ~= "_G" and type(_G[_sName]) == "table" then
		_G[_sName] = nil
	end
end

function os.sleep( _nTime )
	sleep( _nTime )
end

function KilOS.loadAPIsFrom(sPath)
	if not fs.exists( sPath ) or not fs.isDir( sPath ) then return false end
	local tApis = fs.list( sPath )
	for n,sFile in ipairs( tApis ) do
		if string.sub( sFile, 1, 1 ) ~= "." then
			local sPath = fs.combine( sPath, sFile )
			if not fs.isDir( sPath ) then
				os.loadAPI( sPath, true )
			end
		end
	end
end


KilOS.loadAPIsFrom("/rom/apis")
--KilOS.loadAPIsFrom("/lib")
KilOS.loadAPIsFrom("/usr/lib")

if turtle then
	KilOS.loadAPIsFrom("/rom/apis/turtle")
end

local s, m = pcall (function() os.run({}, "/bin/KiloShell.lua"))
if not then 
  printError("KiloShell crashed or was terminated. The computer was shut down")
end