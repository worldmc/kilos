--[[
  The core os file
  
--]]

-- some defines. these are not needed to change usually

local VERSION = "Pre-Alpha 2.2"
local COREAPIS = {"redstone", "peripheral", "bit", "coroutine", "math", "fs", "os", "string", "table"}
local updateurl = "http://stiepen.bplaced.net/KilOS/"
--local updateurl = "http://localhost/KilOSupdater/" --Was for Testing :P

-- copying core apis

for k = 1, #COREAPIS do
  local v = COREAPIS[k]
  rawset(_G, "_"..v, _G[v])
end

-- modifying some core apis


-- core functions

local function getVersion()
  return VERSION
end

local function getUpdateUrl()
  return updateurl
end

rawset(_G, "KilOS", {getVersion = getVersion, getUpdateUrl = getUpdateUrl})

function getAPI(file)
  local tApi = {}
  setmetatable(tApi, {__index = _G})
  os.run(tApi, file)
  return tApi
end

-- loading internal apis

local apis = fs.list("/lib/")
for k = 1, #apis do
  local fname = "/lib/"..apis[k]
  local name = fs.getName(fname)
  if fname:sub(-4, -1) == ".lua" and #fname > 4 then
    name = name:sub(1, -5)
  end
  rawset(_G, name, getAPI(fname))
end

-- loading Kernel Modules

if fs.exists("/sbin/mod/") then
local mods = fs.list("/sbin/mod/")
for i = 1, #mods do
  local s, m = pcall(function()
    loadfile("/sbin/mod/"..mods[i])
  end)
  if not s then
    error("Failed to load Kernel module "..mods[1]..". error: "..m)
  end
end
end

--check for Updates
if http then
  tweaks.setColor(colors.blue)
  print("Checking for updates...")
  local x, y = term.getCursorPos()
  local step = 1
  local h = http.get(updateurl.."version.txt")
  local ver
  if h then ver = h.readLine() end
  if ver == VERSION then
    tweaks.setColor(colors.green)
    print("Your KilOS is up-to-date.")
  elseif ver then
    tweaks.setColor(colors.red)
    print("There is a newer version avaible: "..ver)
    print("Changes: "..h.readAll())
    print("Run \"update\" to Update.")
  else
    tweaks.setColor(colors.red)
    print("Couldn't retrieve version file somehow. Maybe the update server is offline or the Minecraft Server can't access internet.")
  end
  h.close()
else
  tweaks.setColor(colors.orange)
  print("Couldn't check for Updates, because HTTP API is disabled.")
end

user.init()
user.lock(true)

-- loading shell
local s = sandbox.makeSandbox(user)
local f, m = loadfile("/sbin/sandboxstarter.lua")
setfenv(f, s)
if f then f() else printError(m) end