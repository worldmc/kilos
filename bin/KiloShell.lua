local version = "1.2"
local historyLimit = 30

local parentShell = shell

local bExit = false
local sDir = (parentShell and parentShell.dir()) or "/"
local sPath = (parentShell and parentShell.path()) or ".:/bin:/usr/bin:/rom/programs"
local tEnvVars = {}
local tAliases = (parentShell and parentShell.aliases()) or {}
local pointers = {}
local tProgramStack = {}

local shell = {}
local tEnv = {
  shell = shell
}

local function getShell(pid)
  return shell
end
rawset(_G, "getShell", getShell)

--local textPrompt, bgPrompt, textColour, bgColour, textComplete, bgComplete
local colorcfg = {}
local function setColor(name)
  --pcall(function()
    term.setBackgroundColor(colorcfg["bg"..name])
    term.setTextColor(colorcfg["text"..name])
  --end)
end

if term.isColour() then
	colorcfg = {
	  textGreeting = colors.orange,
	  bgGreeting = colors.black,
	  textPrompt = colours.blue,
	  bgPrompt = colors.black,
	  textColour = colours.white,
	  bgColour = colours.black,
	  textComplete = colors.black,
	  bgComplete = colors.white,
	  textUser = colors.green,
	  bgUser = colors.black,
	}
else
	colorcfg = {
	  textGreeting = colors.white,
	  bgGreeting = colors.black,
	  textPrompt = colours.white,
	  bgPrompt = colors.black,
	  textColour = colours.white,
	  bgColour = colours.black,
	  textComplete = colors.black,
	  bgComplete = colors.white,
	  textUser = colors.white,
	  bgUser = colors.black,
	}
end


local function run( _sCommand, ... )
	local sPath = shell.resolveProgram( _sCommand )
	if sPath ~= nil then
		tProgramStack[#tProgramStack + 1] = sPath
   		local result = os.run( tEnv, sPath, ... )
		tProgramStack[#tProgramStack] = nil
		return result
   	else
    	return false, "No such program"
    end
end

local function runLine( _sLine )
	local tWords = {}
	for match in string.gmatch( _sLine, "[^ \t]+" ) do
		table.insert( tWords, match )
	end

	local sCommand = tWords[1]
	if sCommand then
		return run( sCommand, unpack( tWords, 2 ) )
	end
	return false
end

setmetatable(tEnv, {__index = _G})

function shell.run( ... )
	return runLine( table.concat( { ... }, " " ) )
end

function shell.exit()
    bExit = true
end

function shell.dir()
	return sDir
end

function shell.setEnvVar(var, value)
  if type(var) ~= "string" then
    error("parameter #1: string expected, got "..type(var))
  end
  if type(value) ~= "string" then
    error("parameter #2: string expected, got "..type(value))
  end
  tEnvVars[var] = value
end

function shell.getEnvVar(var, default, set)
  if not tEnvVars[var] then
    if set then
      shell.setEnvVar(var, default)
    else
      return default
    end
  end
  return tEnvVars[var]
end

function shell.setDir( _sDir )
	sDir = _sDir
	if sDir:sub(1,1) ~= "/" then sDir = "/"..sDir end
end

function shell.path()
	return sPath
end

function shell.setPath( _sPath )
	sPath = _sPath
end

function shell.resolve( _sPath )
	if _sPath:sub(1,2) == "/~" then
	  _sPath = _sPath:sub(2)
	end
	local sStartChar = string.sub( _sPath, 1, 1 )
	if sStartChar == "~" and _sPath:sub(2,2) == "/" then
	  _sPath = user.getHome().._sPath:sub(2)
	end
	if sStartChar == "/" or sStartChar == "\\" then
		return fs.combine( "", _sPath )
	else
		return fs.combine( sDir, _sPath )
	end
end

function shell.getDisplayName (_sPath)
	local home = user.getHome()
	if _sPath:sub(1,1) ~= "/" then _sPath = "/".._sPath end
	if _sPath:sub(1, #home) == home then
		_sPath = "~".._sPath:sub(#home + 1)
	end
	
	return _sPath--:sub(2)
end

function shell.resolveProgram( _sCommand )
	-- Substitute aliases firsts
	if tAliases[ _sCommand ] ~= nil then
		_sCommand = tAliases[ _sCommand ]
	end

    -- If the path is a global path, use it directly
    local sStartChar = string.sub( _sCommand, 1, 1 )
    if sStartChar == "/" or sStartChar == "\\" then
    	local sPath = fs.combine( "", _sCommand )
    	if fs.exists( sPath ) and not fs.isDir( sPath ) then
			return sPath
    	end
    	sPath = sPath..".lua"
    	if fs.exists( sPath ) and not fs.isDir( sPath ) then
    			return sPath
    	end
		return nil
    end
    
 	-- Otherwise, look on the path variable
    for sPath in string.gmatch(sPath, "[^:]+") do
    	sPath = fs.combine( shell.resolve( sPath ), _sCommand )
    	if fs.exists( sPath ) and not fs.isDir( sPath ) then
			return sPath
    	end
	sPath = sPath..".lua"
    	if fs.exists( sPath ) and not fs.isDir( sPath ) then
    			return sPath
    	end
    end	
	-- Not found
	return nil
end

function shell.programs( _bIncludeHidden, _bIncludeAlias )
	local tItems = {}
	
	-- Add programs from the path
    for sPath in string.gmatch(sPath, "[^:]+") do
    	sPath = shell.resolve( sPath )
		if fs.isDir( sPath ) then
			local tList = fs.list( sPath )
			for n,sFile in pairs( tList ) do
				if not fs.isDir( fs.combine( sPath, sFile ) ) and
				   (_bIncludeHidden or string.sub( sFile, 1, 1 ) ~= ".") then
				   	if #sFile > 4 and sFile:sub(-4, -1) == ".lua" then
						tItems[ sFile:sub(1, -5) ] = true
					else
						tItems[ sFile ] = true
					end
				end
			end
		end
    end	
    if _bIncludeAliases then
    	for k, v in pairs(tAliases) do
    		tItems[k] = true
    	end
    end

	-- Sort and return
	local tItemList = {}
	for sItem, b in pairs( tItems ) do
		table.insert( tItemList, sItem )
	end
	table.sort( tItemList )
	return tItemList
end

function shell.getRunningProgram()
	if #tProgramStack > 0 then
		return tProgramStack[#tProgramStack]
	end
	return nil
end

function shell.setAlias( _sCommand, _sProgram )
	tAliases[ _sCommand ] = _sProgram
end

function shell.clearAlias( _sCommand )
	tAliases[ _sCommand ] = nil
end

function shell.aliases()
	-- Add aliases
	local tCopy = {}
	for sAlias, sCommand in pairs( tAliases ) do
		tCopy[sAlias] = sCommand
	end
	return tCopy
end

function shell.addPointer(from, to)
	pointers[from] = to
end

local tHistory = {}
local cplstr = nil
local progs = {}

local function getCompleteString(text)
  if text == "" then return end
  for a = 1, #progs do
    if #progs[a] > #text then
      if progs[a]:sub(1, text:len()) == text then
        return progs[a]
      end
    end
  end
end

local function charCallBack(sEvent, param, nPos, sLine, redraw, x, y)
  if sEvent == "key" then
    if param == keys.tab then
      --complete
      if cplstr then 
        local pos, line
        line = cplstr.." "
        pos = cplstr:len() + 1
        return pos, line
      end
    end
  end
end

local function redrawCallBack(cc, redraw2, nPos, sLine, x, y)
  write((" "):rep((cplstr or ""):len()))
  if sLine == "" then 
    cplstr = nil  
    term.setCursorPos(x, y)
    --write((" "):rep((cplstr or ""):len()))
    redraw2(cc)
    return 
  end
  cplstr = getCompleteString(sLine:sub(1,nPos + 1))
  redraw2(cc)
  if not cplstr then return end
  local missing = cplstr:sub(nPos + 1)
  term.setCursorPos(x + nPos, y)
  if not cc then
    setColor("Complete")
    write(missing)
  else
    setColor("Colour")
    write(cc:rep(#missing))
  end
  term.setCursorPos(x + nPos, y)
  setColor("Colour")
end

if turtle then 
  sPath = sPath..":/rom/programs/turtle"
end
if http then 
  sPath = sPath..":/rom/programs/http"
end
if term.isColor() then
  sPath = sPath..":/rom/programs/color"
end

shell.setAlias( "ls", "list" )
shell.setAlias( "cp", "copy" )
shell.setAlias( "mv", "move" )
shell.setAlias( "rn", "rename" )
shell.setAlias( "rm", "delete" )

shell.addPointer("dir", "ls")
shell.addPointer("del", "rm")

setColor("Greeting")
print("KiloShell version "..version.. ", running on KilOS "..KilOS.getVersion())
print("Type \"help\" to get started")


for sPath in shell.getEnvVar("startup", "", true):gmatch('([^:]+)') do
  if fs.isDir(sPath) then
    local list = fs.list(sPath)
    for i=1, #list do
      local s, m = shell.run(sPath..list[i])
      if not s then 
        printError("Failed to run "..sPath..". error: "..m)
      end
    end
  else
    local s, m = shell.run(sPath)
    if not s then 
      printError("Failed to run "..sPath..". error: "..m)
    end
  end
end

if fs.exists("/.startup") then
  local s, m = shell.run("/.startup")
  if m and not s then 
    printError("Failed to run startup: "..m)
  end
end

shell.setDir(user.getHome())
shell.run("~/.login")

while not bExit do
  progs = shell.programs(false, true)
  setColor("User")
  write(user.getName())
  setColor("Prompt")
  write("@")
  setColor("User")
  write((os.getComputerLabel() or "unnamed"))
  setColor("Prompt")
  write(":")
  setColor("User")
  write(shell.getDisplayName(sDir))
  setColor("Prompt")
  write("# ")
  setColor("Colour")
  line = tweaks.read(nil, tHistory, charCallBack, redrawCallBack)
  if tHistory[1] ~= line then
    table.insert(tHistory, line)
  end
  if #tHistory > historyLimit then
    table.remove(tHistory, 1)
  end
  local s, m = runLine(line)
  if m and not s then
    local cmd = line:match("[^ ]+")
    local p = pointers[cmd] 
    if p then
      --print(cmd.." wasn't found. Maybe you meant "..p)
      tweaks.setColor(colors.yellow)
      write(cmd)
      tweaks.setColor(colors.blue)
      write(" was not found. Did you mean ")
      tweaks.setColor(colors.yellow)
      write(p)
      tweaks.setColor(colors.blue)
      print(" instead?")
    else
      printError(m)
    end
  end
end