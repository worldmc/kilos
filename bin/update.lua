--local url = "http://stiepen.bplaced.net/KilOS/"
--local url = "http://localhost/KilOSupdater/"
local url = KilOS.getUpdateUrl()

--[[local function createDirR(dir)
  print("creating dir "..dir)
  if dir == "/" or not dir then return end
  if fs.exists(dir) then
    return
  end
  if dir:sub(-1, -1) == "/" then dir = dir:sub(1, -2) end
  createDirR(dir:sub(1, #fs.getName(dir) + 3))
  fs.makeDir(dir)
end--]]

function main()
if http then
  print("Downloading Filelist...")
  local list = url.."list.php?osver="..textutils.urlEncode(KilOS.getVersion()).."&dispver=1"
  print("File list: "..list)
  local h = http.get(list)
  local files = {}
  if not h or h.getResponseCode() ~= 200 then 
    printError("Somehow the file list couldn't be retrieved")
    return
  end
  while true do
    local l = h.readLine()
    if not l then break end
    table.insert(files, l)
  end
  h.close()
  print()
  print("Done. "..#files.." files to download")
  local x, y = term.getCursorPos()
  
  local nscroll = term.scroll
  local function scroll_(n)
    y = y - n
    nscroll(n)
  end
  rawset(term, "scroll", scroll_)
  local s, m = pcall(function()
    for a = 1, #files do
      term.setCursorPos(x, y)
      write("Downloading File "..a.." of "..#files.."...")
      h = http.get(url.."files/"..files[a])
      local dir = files[a]:sub(1, (#fs.getName(files[a]) + 2) * (-1))
      if not (dir == "" or dir == "/" or fs.exists(dir)) then
        fs.makeDir(dir)
      end
      local f = fs.open(files[a], "w") 
      if not f then error("Couldn't open target file.") end
      if not h then error("Couldn't download file.") end
      f.write(h.readAll())
      f.close()
      h.close()
    end
  end)
  rawset(term, "scroll", nscroll)
  print()
  if s then
    print("Done, Press any key to reboot")
    os.pullEvent("key")
    os.reboot()
  else
    printError("Update failed due to internal error: "..m)
  end
else
  printError("HTTP API needs to be enabled for this to work")
end
end
main()