local tArgs = {...}
if #tArgs > 1 then
  tweaks.setColor(colors.yellow, colors.black)
  print("Usage: [<user>]")
else
  local p = ""
  if #tArgs == 0 and user.needsPass(user.getName()) then
    write("Enter current password: ")
    p = read("*")
  end
  write("Enter new password: ")
  local p1 = read("*")
  write("Reenter new password: ")
  local p2 = read("*")
  if p1 == p2 then
    if #tArgs == 1 then
      user.setPass(tArgs[1], p1)
    else
      user.setPassOwn(p, p1)
    end
    print("Done.")
  else
    print("The passwords don't match")
  end
end